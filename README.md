<h1>SOAL 2</h1>
<hr>
<h2>Main Function</h2>
<hr>

    int main(){
        char path[1001];
        pid_t child_id;
        int status = 0;
        
        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }
        
        if (child_id == 0) {
            char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "drakor", NULL};
            execv("/bin/unzip", argv);
        } 
        else {
            while(wait(&status) > 0);
            do_check("drakor");
        }
        return 0;
    }
<p>
        Pada main function, dilakukan pembuatan child_id dan dilakukan fork terhadapnya lalu menjalankan perintah.
        Apabila child_id = 0. Yang berarti sedang bekerja pada child, dilakukan extract terhadap drakor.zip dan dibuat
        directory drakor. Lalu, akan dilakukan do_check("drakor").
</p>
<hr>
<h2>Functions</h2>
<hr>
<p>
    Alur program setelah main, akan masuk pada do_check(string), lalu akan dilakukan storing terhadap path.
    Selanjutnya akan dilakukan perulangan file pada directory. Ketika isi dari dp->name tidak "." atau ".." yang berarti kosong,
    maka akan dilakukan store nama file, lalu akan diberikan token separator ";_". Selanjutnya akan di store kedalam temporary
    array 2D dengan [][j] pada j = 0 berisikan judul, j = 1 berisikan tahun rilis, dan j = 2 berisikan genre film.
    Selanjutnya akan dilakukan looping pada info yang selanjutnya akan dilakukan pemanggilan fungsi untuk
    membuat directory, lalu mengcopy file, lalu membuat info, dan setelah loop tersebut akan dilakukan penghapusan
    pada file.png yang telah di proses pada rootnya yaitu pada folder drakor.

    Kendala yang kami alami untuk soal ini adalah belum bisa membuat text file print text secara ascending.
</p>
<hr>
<h2>Screenshot Hasil Eksekusi program</h2>
![drakor](/uploads/9e4b43b970f6eb1e49f0eea63c3c9bf0/drakor.PNG)
![drakor_1](/uploads/ab3d5e0ebcd5129644a61fcfe2bb6855/drakor_1.PNG)
![drakor_2](/uploads/a41db494d9a4a84e3aa4769afa5c389b/drakor_2.PNG)
![drakor_3](/uploads/50dfa9c28d88f54a850b8de2261cecba/drakor_3.PNG)
<hr>
<h1>Soal 3</h1>
<hr>
<p>
Pada soal ini, ide kami adalah dengan melakukan extract file berekstensi .jpg pada "animal.zip".
Lalu membuat sub-folder darat, air, dan bird. Setelah itu memindahkan file.jpg kedalam sub-folder sesuai dengan informasi yang didapat dari file.jpg. Serta membuat file keterangan dengan nama list.txt

Setelah semua pada posisinya, akan dihapus file yang tidak memiliki keterangan. Juga akan dihapus folder bird karena tidak diperlukan.

Kendala kami untuk soal ini adalah membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air”
ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana
UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

kita masih belum berhasil untuk bagian ini.

</p>
<hr>
<h2>Screenshot Hasil Eksekusi program</h2>
![WhatsApp_Image_2022-03-27_at_1.51.58_PM](/uploads/806e683e43c250fe3463867566175411/WhatsApp_Image_2022-03-27_at_1.51.58_PM.jpeg)
![WhatsApp_Image_2022-03-27_at_1.52.30_PM](/uploads/b6d423cf00818f489861dfee7578dc1a/WhatsApp_Image_2022-03-27_at_1.52.30_PM.jpeg)
![WhatsApp_Image_2022-03-27_at_1.52.51_PM](/uploads/e0926d6d50d51315faeb45b8e9799bcd/WhatsApp_Image_2022-03-27_at_1.52.51_PM.jpeg)
<hr>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <dirent.h>
#include <string.h>
#include <json-c/json.h>


void downloadDatabase(){
    char *argv1[6] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "Anggap_ini_database_characters.zip", NULL};
    char *argv2[6] = {"wget", "--no-check-certificate", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "Anggap_ini_database_weapon.zip", NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child > 0){
        id_child = fork();

        if(id_child < 0){
            exit(0);
        }else if(id_child > 0){
            
        }else{
            execv("/usr/bin/wget", argv1);
        }
        
    }else{
        execv("/usr/bin/wget", argv2); 
    }
}

void extractZIP(){
    char *argv1[3] = {"unzip", "Anggap_ini_database_characters.zip", NULL};
    char *argv2[3] = {"unzip", "Anggap_ini_database_weapon.zip", NULL};
    
    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child > 0){
        id_child = fork();

        if(id_child < 0){
            exit(0);
        }else if(id_child > 0){
            
        }else{
            execv("/usr/bin/unzip", argv1);
        }
        
    }else{
        execv("/usr/bin/unzip", argv2); 
    }
}

void createFolder(char path[]){
    char *argv[3] = {"mkdir", path, NULL};

    pid_t id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child > 0){
        
    }else{
        execv("/usr/bin/mkdir", argv); 
    }
}


int countFile(char directoryPath[]){
    int fileCount = 0;

    //Menghitung file
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);

    while(file != NULL){
        if(file->d_type == DT_REG){
            fileCount++;
        }
        file = readdir(path);
    }

    closedir(path);

    return fileCount;
}

struct json_object** loadFile(char directoryPath[], int fileCount){
    int i = 0;
    DIR *path = opendir(directoryPath);
    struct dirent *file = readdir(path);
    
    char buffer[5120];
    struct json_object **parsed_json = malloc(sizeof(struct json_object*) * fileCount);
    char filePath[300];

     while(file != NULL){
        strcpy(filePath, directoryPath);

        if(file->d_type == DT_REG){
            
            strcat(filePath, file->d_name);
        
            FILE *fp;
            fp = fopen(filePath, "r");
            fread(buffer, 5120, 1, fp);
            fclose(fp);

            *(parsed_json + i) = json_tokener_parse(buffer);
            
            i++;
        }
        file = readdir(path);
    }

    closedir(path);

    return parsed_json;
}

int getRandomIndex(int low, int high){
    int index = (rand() %(high - low + 1)) + low;
    return index;
}

int main(){

    pid_t id_child;
    int status;

    id_child = fork();

    if(id_child < 0){
        exit(0);
    }else if(id_child == 0){
        downloadDatabase();
    }else{
        sleep(5);
        extractZIP();
    }
    
    createFolder("/home/muhammad/gacha_gacha");

    sleep(2);

    //Mengimpor file JSON
    char charactersPath[] = "/home/muhammad/PraktikumSisop/Modul2/soal1/characters/";
    char weaponsPath[] = "/home/muhammad/PraktikumSisop/Modul2/soal1/weapons/";
    int characterCount;
    int weaponCount;

    struct json_object **parsed_characters;
    struct json_object **parsed_weapons;

    characterCount = countFile(charactersPath);
    parsed_characters = loadFile(charactersPath, characterCount);

    weaponCount = countFile(weaponsPath);
    parsed_weapons = loadFile(weaponsPath, weaponCount);

    sleep(2);

    //Memulai gacha
    int iteration = 0;    
    int primogems = 79000;
    int killLoop = 0;
    srand(time(NULL));

    char type[12];
    char resultList[11][2000];
    int index;
    char underscore[2] = "_";
    char newLine[3] = "\n";
    char iterationChar[6];
    char primogemsChar[6];
    struct json_object *name;
    struct json_object *rarity;

    //Membuat file .txt pertama untuk memulai
    FILE *fp;

    time_t current;
    struct tm *curr_time;
    int hour, minute, second;
    char hourStr[3], minStr[3], secStr[3];

    current = time(NULL);
    curr_time = localtime(&current);
    hour = curr_time->tm_hour;
    minute = curr_time->tm_min;
    second = curr_time->tm_sec;

    sprintf(hourStr, "%d", hour);
    sprintf(minStr, "%d", minute);
    sprintf(secStr, "%d", second);

    char semiColon[2] = ":";
    char underScore[2] = "_";
    char dottxt[5] = ".txt";

    char path[200] = "/home/muhammad/gacha_gacha/"; 
    int tempIt = iteration + 1;
    char tempItStr[6];
    sprintf(tempItStr, "%d", tempIt);

    strcat(path, hourStr);
    strcat(path, semiColon);
    strcat(path, minStr);
    strcat(path, semiColon);
    strcat(path, secStr);
    strcat(path, semiColon);
    strcat(path, underScore);
    strcat(path, "gacha");
    strcat(path, underScore);
    strcat(path, tempItStr);
    strcat(path, dottxt);

    fp = fopen(path, "w");
    
    char prevPath[200];
    strcpy(prevPath, path);

    fclose(fp);

    sleep(1);

    while(1){

        if(primogems >= 160 && iteration != 0){
            primogems -= 160;
            sprintf(iterationChar, "%d", iteration);
            sprintf(primogemsChar, "%d", primogems);

            if(iteration%2 == 0){
                index = getRandomIndex(0, weaponCount - 1);
                json_object_object_get_ex(*(parsed_weapons + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_weapons + index), "name", &name);
                strcpy(type, "weapons");
            }else if(iteration%2 == 1){
                index = getRandomIndex(0, characterCount - 1);
                json_object_object_get_ex(*(parsed_characters + index), "rarity", &rarity);
                json_object_object_get_ex(*(parsed_characters + index), "name", &name);
                strcpy(type, "characters");
            }

            strcpy(resultList[(iteration-1) % 10], iterationChar);
            strcat(resultList[(iteration-1) % 10], underscore);
            strcat(resultList[(iteration-1) % 10], type);
            strcat(resultList[(iteration-1) % 10], underscore);
            strcat(resultList[(iteration-1) % 10], json_object_get_string(rarity));
            strcat(resultList[(iteration-1) % 10], underscore);
            strcat(resultList[(iteration-1) % 10], json_object_get_string(name));
            strcat(resultList[(iteration-1) % 10], underscore);
            strcat(resultList[(iteration-1) % 10], primogemsChar);
            strcat(resultList[(iteration-1) % 10], newLine);
            
            
        }else if(primogems < 160 && iteration != 0){
            killLoop = 1;
        }

        if(iteration % 10 == 0 && iteration != 0){
            //Memasukkan nilai ke .txt sebelumnya

            FILE *fp1;
            fp1 = fopen(prevPath, "w");
            
            int i;
            for(i = 0; i < 10; i++){
                fprintf(fp1, "%s", resultList[i]);
            }

            sleep(1);

            //Membuat file .txt selanjutnya
        
            FILE *fp2;

            current = time(NULL);
            curr_time = localtime(&current);
            hour = curr_time->tm_hour;
            minute = curr_time->tm_min;
            second = curr_time->tm_sec;

            sprintf(hourStr, "%d", hour);
            sprintf(minStr, "%d", minute);
            sprintf(secStr, "%d", second);

            strcpy(path, "/home/muhammad/gacha_gacha/");
            
            tempIt = iteration + 1;

            sprintf(tempItStr, "%d", tempIt);

            strcat(path, hourStr);
            strcat(path, semiColon);
            strcat(path, minStr);
            strcat(path, semiColon);
            strcat(path, secStr);
            strcat(path, semiColon);
            strcat(path, underScore);
            strcat(path, "gacha");
            strcat(path, underScore);
            strcat(path, tempItStr);
            strcat(path, dottxt);

            fp = fopen(path, "w");

            strcpy(prevPath, path);

            fclose(fp);
        }

        if(iteration % 90 == 0 && iteration != 0){
            int tempIt = iteration;
            char tempItStr[6];
            sprintf(tempItStr, "%d", tempIt);

            char folderPath[200] = "/home/muhammad/gacha_gacha/total_gacha_";
            strcpy(folderPath, tempItStr);
            createFolder(folderPath);

            char *argv[4] = {"mv", "/home/muhammad/gacha_gacha/*.txt", "/home/muhammad/gacha_gacha/total_gacha_*", NULL};

            pid_t id_child = fork();

            if(id_child < 0){
                exit(0);
            }else if(id_child > 0){
                
            }else{
                execv("/usr/bin/mv", argv); 
            }

        }
        

        if(killLoop) break;
        iteration++;

        if(hour == 7 && minute == 44){
            char *argv[4] = {"zip", "/home/muhammad/gacha_gacha/not_safe_for_wibu", "/home/muhammad/gacha_gacha/total_gacha_*", NULL};

            pid_t id_child = fork();

            if(id_child < 0){
                exit(0);
            }else if(id_child > 0){
                
            }else{
                execv("/usr/bin/zip", argv); 
            }
        }
    }
    
}
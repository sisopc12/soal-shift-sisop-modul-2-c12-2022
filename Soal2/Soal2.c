/*
Achmad Nashruddin Riskynanda
Muhammad Rafif Fadhil Naufal
Aping

C12 C12 C12
Soal2
*/

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>
#include <stdlib.h>

void lets_remove_rootfile(char *root_file);
void lets_create_infos(char *dir_name, char *mov_title, char *rel_years, int iter, int num);
void lets_copy_files(char *dir_name, char *new_name, char *root_file_name );
void lets_create_dir(char* dir_name);
void do_check(char *basePath);


int main(){
    char path[1001];
    pid_t child_id;
    int status = 0;

    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
            char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "drakor", NULL};
            execv("/bin/unzip", argv);
    } 
    else {
            while(wait(&status) > 0);
            do_check("drakor");
        }
return 0;
}

void do_check(char *basePath){
    char path[1001];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    char *token;
    if (!dir)
        return;
    
    while ((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char name[1001];
            char *str;
            str = dp->d_name;
            
            snprintf(name, sizeof name, "%s", dp->d_name);
            token  = strtok(name, ";_");

            char *info[2][3];
            int counter=0;
            while (token != NULL){
                int data=0;
                while(data<3){
                    info[counter][data] = token;
                    // printf("%s ", token);
                    token = strtok(NULL, ";_");
                    data++;
                }counter++;
            }
            char* years;
            for(int i = 0; i < counter; i++){
                // printf("%s : %s : %s\n", info[i][0], info[i][1], info[i][2]);
                lets_create_dir(info[i][2]);
                lets_copy_files(info[i][2], info[i][0], str);
                lets_create_infos(info[i][2], info[i][0], info[i][1], i, counter);
                
            }
        lets_remove_rootfile(str);
            
        }
    }
closedir(dir);
}

void lets_create_dir(char* dir_name){
    int dir_length = strlen(dir_name);

    if(dir_name[dir_length - 4] == '.'){
        dir_name[dir_length - 4] = '\0';
        dir_name[dir_length - 3] = '\0';
        dir_name[dir_length - 2] = '\0';
        dir_name[dir_length - 1] = '\0';
    }
    int status = 0;
    char buf1[1001];
    // printf("%s\n", dir_name);
    // printf("%s\n", buf1);
    if(fork()==0){
        char buf1[256];
        snprintf(buf1, sizeof buf1, "drakor/%s", dir_name);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}

void lets_copy_files(char *dir_name, char *new_name, char *root_file_name ){
    int dir_length = strlen(dir_name);

    if(dir_name[dir_length - 4] == '.'){
        dir_name[dir_length - 4] = '\0';
        dir_name[dir_length - 3] = '\0';
        dir_name[dir_length - 2] = '\0';
        dir_name[dir_length - 1] = '\0';
    }
    int status = 0;
    char buf1[1001];
    char buf2[1001];
    snprintf(buf1, sizeof buf1, "drakor/%s", root_file_name);
    snprintf(buf2, sizeof buf2, "drakor/%s/%s.png", dir_name, new_name);
    printf("%s\n", buf1);
    printf("%s\n", buf2);
    if(fork()==0){
        char *argv[] = {"cp", buf1, buf2, NULL};
        execv("/bin/cp", argv);
    }
    while(wait(&status)>0);
}
void lets_create_infos(char *dir_name, char *mov_title, char *rel_years, int iter, int num){
    int dir_length = strlen(dir_name);

    if(dir_name[dir_length - 4] == '.'){
        dir_name[dir_length - 4] = '\0';
        dir_name[dir_length - 3] = '\0';
        dir_name[dir_length - 2] = '\0';
        dir_name[dir_length - 1] = '\0';
    }

    char buf1[10000];
    snprintf(buf1, sizeof buf1, "drakor/%s/keterangan.txt", dir_name);
    printf("%s\n", buf1);
    FILE *keterangan;
    keterangan = fopen(buf1, "a+");
    fprintf(keterangan, "title : %s\n", mov_title);
    fprintf(keterangan, "rilis : %s\n\n", rel_years);
    fclose(keterangan);
}

void lets_remove_rootfile(char *root_file){
    char buf1[1001];
    snprintf(buf1, sizeof buf1, "drakor/%s", root_file);
    if(fork()==0){
        char *argv[] = {"rm", buf1, NULL};
        execv("/bin/rm", argv);
    }
}

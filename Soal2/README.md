<h1>SOAL 2</h1>
<hr>
<h2>Main Function</h2>
<hr>

    int main(){
        char path[1001];
        pid_t child_id;
        int status = 0;
        
        child_id = fork();
        if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
        }
        
        if (child_id == 0) {
            char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "drakor", NULL};
            execv("/bin/unzip", argv);
        } 
        else {
            while(wait(&status) > 0);
            do_check("drakor");
        }
        return 0;
    }
<p>
        Pada main function, dilakukan pembuatan child_id dan dilakukan fork terhadapnya lalu menjalankan perintah.
        Apabila child_id = 0. Yang berarti sedang bekerja pada child, dilakukan extract terhadap drakor.zip dan dibuat
        directory drakor. Lalu, akan dilakukan do_check("drakor").
</p>
<hr>
<h2>Functions</h2>
<hr>
<p>
    Alur program setelah main, akan masuk pada do_check(string), lalu akan dilakukan storing terhadap path.
    Selanjutnya akan dilakukan perulangan file pada directory. Ketika isi dari dp->name tidak "." atau ".." yang berarti kosong,
    maka akan dilakukan store nama file, lalu akan diberikan token separator ";_". Selanjutnya akan di store kedalam temporary
    array 2D dengan [][j] pada j = 0 berisikan judul, j = 1 berisikan tahun rilis, dan j = 2 berisikan genre film.
    Selanjutnya akan dilakukan looping pada info yang selanjutnya akan dilakukan pemanggilan fungsi untuk
    membuat directory, lalu mengcopy file, lalu membuat info, dan setelah loop tersebut akan dilakukan penghapusan
    pada file.png yang telah di proses pada rootnya yaitu pada folder drakor.
</p>

<h1>Soal 3</h1>
<hr>
Pada soal ini, ide kami adalah dengan melakukan extract file berekstensi .jpg pada "animal.zip".
Lalu membuat sub-folder darat, air, dan bird. Setelah itu memindahkan file.jpg kedalam sub-folder sesuai dengan informasi yang didapat dari file.jpg. Serta membuat file keterangan dengan nama list.txt

Setelah semua pada posisinya, akan dihapus file yang tidak memiliki keterangan. Juga akan dihapus folder bird karena tidak diperlukan.

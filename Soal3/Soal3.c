/*
Achmad Nashruddin Riskynanda
M Rafif
Afril M

C12
SOAL3
*/

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <wait.h>
#include <stdlib.h>

void lets_create_infos(char *dir_name, char *animal_name, char *type);
void lets_remove_rootfile(char *root_file);
void lets_copy_files(char *dir_name, char *new_name, char *root_file_name );
void lets_create_dir(char *dir_name);
int file_counter(char *root_dir);
void do_check(char *basePath);

int main(){
    char path[1001];
    pid_t child_id;
    int status = 0;

    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
            char *argv[] = {"unzip", "animal.zip", "*.jpg", NULL};
            execv("/bin/unzip", argv);
    } 
    else {
            while(wait(&status) > 0);
            do_check("animal");
        }
return 0;
}
int file_counter(char *root_dir){
    struct dirent *dp;
    DIR *dir = opendir(root_dir);
    int counter = 0;
    if (!dir)
        return 0;
    
    while ((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0){
            continue;
        }
        if(dp->d_name[strlen(dp->d_name) - 4] == '.'){
            counter++;
        }
    }
    return counter;
}
void do_check(char *root_dir){
    char path[1001];
    struct dirent *dp;
    DIR *dir = opendir(root_dir);
    int file_count = file_counter("animal");
    printf("\n\n%d\n", file_count);

    // create directory 
    lets_create_dir("darat");
    lets_create_dir("air");
    lets_create_dir("bird");

    char *data[file_count][4];
    char *token;
    
    if(!dir) return;

    int index = 0;
    while ((dp = readdir(dir)) != NULL){
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0){
            continue;
        }
        if(dp->d_name[strlen(dp->d_name) - 4] != '.'){
            continue;
        }
        
        char file_title[1000];
        char *str;
        str = dp->d_name;
        data[index][3] = dp->d_name;
        snprintf(file_title, sizeof file_title, "%s", dp->d_name);
        token = strtok(file_title, "_");
        while(token != NULL){
            int d = 0;
            // printf("%d : ", index);
            while(d<3){
                
                data[index][d] = token;
                // printf(" %s", data[index][d]);
                token = strtok(NULL, "_");
                d++;
            }
            // printf("\n");
            
        }
        //copy files
        if(data[index][1] == NULL) {
            printf("REMOVE :: %s\n", data[index][3]);
            lets_remove_rootfile(data[index][3]);
            continue;
        }
        lets_copy_files(data[index][1],data[index][0], data[index][3]);
        lets_create_infos(data[index][1],data[index][0],data[index][1]);
        lets_remove_rootfile(data[index][3]);
        
        index++;
    }

    //HAPUS BURUNG
    lets_remove_bird_dir();

    printf("\n\nEND\n\n");
    
closedir(dir);
}

void lets_create_dir(char *dir_name){
    int status = 0;
    if(fork()==0){
        char buf1[256];
        snprintf(buf1, sizeof buf1, "animal/%s", dir_name);
        char *argv[] = {"mkdir", "-p", buf1, NULL};
        execv("/bin/mkdir", argv);
    }
    while(wait(&status)>0);
}

// Copy file ke folder tujuan
void lets_copy_files(char *dir_name, char *new_name, char *root_file_name ){
    int dir_length = strlen(dir_name);

    if(dir_name[dir_length - 4] == '.'){
        dir_name[dir_length - 4] = '\0';
        dir_name[dir_length - 3] = '\0';
        dir_name[dir_length - 2] = '\0';
        dir_name[dir_length - 1] = '\0';
    }
    int status = 0;
    char buf1[1001];
    char buf2[1001];
    snprintf(buf1, sizeof buf1, "animal/%s", root_file_name);
    snprintf(buf2, sizeof buf2, "animal/%s/%s.jpg", dir_name, new_name);
    printf("%s\n", buf1);
    printf("%s\n", buf2);
    if(fork()==0){
        char *argv[] = {"cp", buf1, buf2, NULL};
        execv("/bin/cp", argv);
    }
    while(wait(&status)>0);
}

//Pembuatan Keterangan

void lets_create_infos(char *dir_name, char *animal_name, char *type){
    int dir_length = strlen(dir_name);

    if(dir_name[dir_length - 4] == '.'){
        dir_name[dir_length - 4] = '\0';
        dir_name[dir_length - 3] = '\0';
        dir_name[dir_length - 2] = '\0';
        dir_name[dir_length - 1] = '\0';
    }

    char buf1[10000];
    snprintf(buf1, sizeof buf1, "animal/%s/list.txt", dir_name);
    printf("%s\n", buf1);
    FILE *keterangan;
    keterangan = fopen(buf1, "a+");
    fprintf(keterangan, "name : %s\n", animal_name);
    fprintf(keterangan, "type : %s\n\n", type);
    fclose(keterangan);
}

// Penghapusan File

void lets_remove_rootfile(char *root_file){
    char buf1[1001];
    snprintf(buf1, sizeof buf1, "animal/%s", root_file);
    if(fork()==0){
        char *argv[] = {"rm", buf1, NULL};
        execv("/bin/rm", argv);
    }
}
//penghapusan burung
void lets_remove_bird_dir(){
    if(fork()==0){
        char *argv[] = {"rm","-r", "animal/bird", NULL};
        execv("/bin/rm", argv);
    }
}